<?php

/**
 * Implementation of hook_form_alter().
 *
 * Extend the system information form with more collectables.
 */
function fancy_contacts_contact_form(&$form_state) {
  $defaults = variable_get('fancy_contacts', array());
  $form['fancy_contacts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact details'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -5,
  );

  $contact_options = _fancy_contacts_options();
  foreach ($contact_options as $name => $details) {

    // Phone number does not have cover text.
    if (isset($details['name_description'])) {
      $form['fancy_contacts'][$name]['#tree'] = TRUE;
      $form['fancy_contacts'][$name]['name'] = array(
        '#title' => $details['title'],
        '#description' => $details['name_description'],
        '#type' => 'textfield',
        '#default_value' => isset($defaults[$name]['name']) ? $defaults[$name]['name'] : '',
      );
    }
    $form['fancy_contacts'][$name]['value'] = array(
      '#title' => $details['title'],
      '#description' => $details['description'],
      '#type' => 'textfield',
      '#default_value' => isset($defaults[$name]['value']) ? $defaults[$name]['value'] : '',
    );
  }
  return system_settings_form($form);
}
