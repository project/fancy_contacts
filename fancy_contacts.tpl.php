<?php // $Id$ 
/**
 * @file
 * Default template file for the contact block, possible variables:
 *   $telephone
 *   $email
 *   $twitter
 *   $skype
 *   $facebook_group
 *   $facebook_fan
 */
?>
<div class="contact-details">
  <?php if ($telephone): ?>
  <span class="contact-item telephone"><span class="icon"></span><?php print $telephone; ?></span>
  <?php endif; ?>
  <?php if ($email): ?>
  <span class="contact-item email"><span class="icon"></span><?php print $email; ?></span>
  <?php endif; ?>
  <?php if ($twitter): ?>
  <span class="contact-item twitter"><span class="icon"></span><?php print $twitter; ?></span>
  <?php endif; ?>
  <?php if ($skype): ?>
  <span class="contact-item skype"><span class="icon"></span><?php print $skype; ?></span>
  <?php endif; ?>
  <?php if ($facebook_group): ?>
  <span class="contact-item facebook_group"><span class="icon"></span><?php print $facebook_group; ?></span>
  <?php endif; ?>
  <?php if ($facebook_fan): ?>
  <span class="contact-item facebook_fan"><span class="icon"></span><?php print $facebook_fan; ?></span>
  <?php endif; ?>
</div>
